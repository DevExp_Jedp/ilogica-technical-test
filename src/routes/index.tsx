import React from 'react'
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'

import { Home } from '../pages/home'
import { About } from '../pages/about'
import { Activities } from '../pages/activities'
import { Activity } from '../pages/activity'
import { NotFound } from '../pages/notFound'

export const Router = () => {

    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/home" component={Home} />
                <Route exact path="/nosotros" component={About} />
                <Route exact path="/actividades" component={Activities} />
                <Route exact path="/actividades/:id" component={Activity} />
                <Route path="/not-found" component={NotFound} />
                <Redirect to="/not-found" />
            </Switch>
        </BrowserRouter>
    )
}