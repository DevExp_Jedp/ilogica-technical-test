import React from 'react'
import ReactDOM from 'react-dom'

import { Router } from './routes'

import 'swiper/swiper.scss'
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';

import './assets/styles/main.scss'

ReactDOM.render(<Router />, window.document.getElementById('app'))
