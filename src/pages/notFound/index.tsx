import React from 'react'
import { DefaultLayout } from '../../layouts/default'
import { NotFoundContainer } from '../../containers/404'

export const NotFound:React.FC = () => {

    return (
        <DefaultLayout color="white">
            <NotFoundContainer />
        </DefaultLayout>
    )
}