import React, { useState, useEffect } from "react";
import { MdArrowBack } from 'react-icons/md'
import { useParams } from 'react-router-dom'
import { DefaultLayout } from '../../layouts/default'


import { Link } from '../../components/commons/buttons/link'
import { ImageSlider } from '../../components/sliders/ImageSlider'
import { SocialMediaContainer } from '../../containers/socialMedia'
import { Quote } from '../../components/quote'

import { Activity as ActivityInterface } from "../../utils/types/activity";
import { activities, relatedActivities } from '../../mocks/activity'

import { RelatedActivities } from '../../containers/relatedActivities'

export const Activity:React.FC = () => {

    const { id } = useParams<{id?: string}>()

    const [activity, setActivity] = useState<ActivityInterface | undefined>()
    const [loading, setLoading] = useState<boolean>(true)

    useEffect(() => {
        
        const getData = async () => {
            id && setActivity(activities[Number(id)])
            setLoading(false)
        }

        getData()
    }, [])

    return (
        <DefaultLayout>
            <section className="flex flex-col align-center justify-center activity__information">
                <div className="activity__container activity__title">
                    <h1><Link to="/actividades" color="white">
                            <MdArrowBack />
                            Volver a Actividades
                        </Link></h1>
                </div>
            </section>
            <section className="flex flex-row align-center justify-center activity__blog">

                <div className="activity__container activity__content">

                    <aside className="activity__content-share">
                    </aside>

                    <article className="flex flex-col align-start justify-start activity__content-text">
                        <h1>{activity?.title}</h1>
                        <figure aria-label={activity?.abstract} className="activity__content-text-image">
                            <img alt={activity?.title} src={activity?.cover} />
                        </figure>
                        <small>{activity?.date} - {activity?.time} de lectura</small>
                        <p className="activity__content-description">
                            {activity?.description}
                        </p>
                        {
                            activity?.quote && (
                                <Quote 
                                    text={activity.quote}
                                />
                            )
                        }

                        <div className="activity__content-slider">
                            <ImageSlider>
                                {
                                    activity?.images.map((image, i) => (
                                        <figure key={i} className="activity__content-images">
                                            <img src={image} alt={image}/>
                                        </figure>
                                    ))
                                }
                            </ImageSlider>
                        </div>
                    </article>
                </div>

            </section>

            <section className="flex flex-col align-center justify-center activity__related">
                    <div className="activity__container">
                        <RelatedActivities 
                            activities={relatedActivities}
                        />
                    </div>
            </section>
        </DefaultLayout>
    )

}