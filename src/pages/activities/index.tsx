import React from "react";
import { DefaultLayout } from "../../layouts/default"

import { activtyData } from '../../mocks/activity'

import { ActivityList } from '../../containers/activitesList' 

export const Activities:React.FC = () => {

    return (
        <DefaultLayout>
            <section className="flex flex-col align-center justify-center activities__information">
                <div className="activities__container activities__title">
                    <h1>Actividades</h1>
                </div>

                <div className="activities__container activities__list">
                    <h2>Categorías</h2>
                    <ActivityList data={activtyData} />
                </div>
            </section>
        </DefaultLayout>
    )

}