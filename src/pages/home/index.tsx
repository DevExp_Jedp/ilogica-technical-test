import React from 'react'
import { DefaultLayout } from '../../layouts/default'

import { CoverSlider } from '../../components/sliders/CoverSlider'
import { CoverActivity } from '../../containers/coverActivity'

import { ParagraphWithButton } from '../../containers/paragraph/withButton'
import { ParagraphNormal } from '../../containers/paragraph/normal'
import { AdvantagesContainer } from '../../containers/advantages'
import { PartnersSliderContainer } from '../../containers/partnersSlider'
import { Image } from '../../components/images/image'

export const Home:React.FC = () => {

    return (
        <DefaultLayout color="white">
            <CoverSlider>
                <CoverActivity
                    id={1}
                    abstract="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, quasi enim ut recusandae sit atque amet voluptas quos quisquam aspernatur!"                
                    image="public/images/backgrounds/bg-1.jpg"
                    title="Lorem Ipsum Dolor Sit"
                />
                <CoverActivity
                    id={2}
                    abstract="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, quasi enim ut recusandae sit atque amet voluptas quos quisquam aspernatur!"                
                    image="public/images/backgrounds/bg-2.jpg"
                    title="Lorem Ipsum Dolor Sit"
                />
                <CoverActivity
                    id={3}
                    abstract="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam, quasi enim ut recusandae sit atque amet voluptas quos quisquam aspernatur!"                
                    image="public/images/backgrounds/bg-3.jpg"
                    title="Lorem Ipsum Dolor Sit"
                />
                
            </CoverSlider>

            <section className="home__categories">
                <div className="home__categories-container">

                    <article className="home__categories-content">
                        <ParagraphWithButton 
                            title="¿Lorem Ipsum?"
                            onClick={() => {}}
                            paragraph="Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa incidunt adipisci et voluptatibus cum id tempora alias explicabo officia quis eaque non, error iure fugiat rem dolorum ab nulla molestias."
                            buttonColor="red"
                        />
                    </article>
                    <figure className="relative flex flex-row flex-wrap align-center justify-center home__categories-images">
                        <div className="flex flex-row align-center justify-center">
                            <Image 
                                alt="activity of the image"
                                src="public/images/backgrounds/bg-4.jpg"
                                caption="Lorem ipsumo dolor"
                            />
                        </div>
                        <div className="flex flex-col align-center justify-center">
                            <Image 
                                alt="activity of the image"
                                src="public/images/backgrounds/bg-5.jpg"
                                caption="Lorem ipsumo dolor"
                            />
                            <Image 
                                alt="activity of the image"
                                src="public/images/backgrounds/bg-6.jpg"
                                caption="Lorem ipsumo dolor"
                            />
                        </div>
                    </figure>
                </div>
            </section>

            <section className="home__advantages">
                <div className="home__container">
                    <AdvantagesContainer />
                </div>
            </section>

            <section className="flex flex-row align-center justify-center home__partners">
                <div className="home__container">
                    <PartnersSliderContainer />
                </div>
            </section>

            <section className="flex flex-row align-center justify-center home__about">
                <div className="home__about-container">
                    <div className="home__about-us">
                        <ParagraphWithButton 
                            title="Lorem Ipsum Dolor Sit"
                            onClick={() => {}}
                            color="white"
                            paragraph="Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa incidunt adipisci et voluptatibus cum id tempora alias explicabo officia quis eaque non, error iure fugiat rem dolorum ab nulla molestias."
                            buttonColor="white"
                        />
                    </div>

                    <div className="home__about-list">
                        <ParagraphNormal color="white" title="Lorem Ipsumo Two">
                            <ul>
                                <li>Hello there</li>
                                <li>Hello there</li>
                                <li>Hello there</li>
                            </ul>
                        </ParagraphNormal>
                    </div>

                </div>
            </section>
        </DefaultLayout>
    )
}