import React from 'react'
import { DefaultLayout } from '../../layouts/default'
import { ContactContainer } from '../../containers/contact'
import { ParagraphNormal } from '../../containers/paragraph/normal'
import { ImageMedium } from '../../components/images/imageMedium'
import { PeopleList } from '../../containers/peopleList'

import { peopleData } from '../../mocks/people'

export const About:React.FC = () => {

    return (
        <DefaultLayout color="white">
            <section className="flex flex-col align-center justify-center about__information">

                <div className="about__container about__title">
                    <h1>Nosotros</h1>
                </div>
                <div className="about__container about__information">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam facere animi esse possimus veniam nobis corporis molestiae sit ad architecto ab provident totam deleniti aspernatur nisi, praesentium atque rem adipisci odio tempore quibusdam consequuntur natus sapiente magnam. Suscipit, quas libero!</p><br />
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corrupti nulla molestias enim velit maxime, suscipit iste aliquam omnis, dolorum iure eos aliquid perspiciatis animi a earum. Odit aperiam officia placeat!</p><br />
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rerum neque quisquam natus?</p><br />
                </div>
            </section>

            <section className="flex flex-col align-center justify-center about__content">
                <div className="about__content-container">
                    <div className="about__content-paragraph">
                        <ParagraphNormal title="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo?">
                            <p>
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laudantium eligendi vel saepe incidunt. Quasi minima saepe voluptates, adipisci, quis dolores porro molestias vel labore laudantium odio alias possimus voluptas eligendi.
                            </p>
                        </ParagraphNormal>
                    </div>
                    <div className="about__content-image">
                        <ImageMedium 
                            alt="About image"
                            src="public/images/backgrounds/bg-5.jpg"
                        />
                    </div>
                </div>
            </section>

            <section className="flex flex-col align-center justify-center about__people">
                <div className="about__container">
                    <ParagraphNormal title="Lorem Ipsum dolor sit amet" />
                    <PeopleList 
                        data={peopleData}
                    />
                </div>
            </section>

            <ContactContainer />
        </DefaultLayout>
    )
}