import React from 'react'
import { Link } from 'react-router-dom'
import { LogoMS, FacebookIcon, InstagramIcon, TwitterIcon, YoutubeIcon } from '../commons/icons'

export const Footer = () => {

    return (
        <footer className="footer">
            <div className="footer__container">
                <Link aria-label="home" to="/" className={`header__logo`}>
                    <LogoMS />
                </Link>
                <address className="footer__address">
                    <ul className="flex flex-row footer__address-list">
                        <li>
                            <a href="https://facebook.com" aria-label="facebook">
                                <FacebookIcon />
                            </a>
                        </li>
                        <li>
                            <a href="https://linkedin.com" aria-label="linkedin">
                                <img src="/public/images/social-media/linkedin.png" alt="linkedin logo"/>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com" aria-label="twitter">
                                <TwitterIcon />
                            </a>
                        </li>
                        <li>
                            <a href="https://isntagram.com" aria-label="instagram">
                                <InstagramIcon />
                            </a>
                        </li>
                        <li>
                            <a href="https://youtube.com" aria-label="youtube">
                                <YoutubeIcon />
                            </a>
                        </li>
                       
                        
                    </ul>
                </address>
            </div>
            <hr className="footer__separator" />
            <div className="footer__container footer__container--second">
                <nav className="footer__nav">
                    <h4 className="footer__title">Sobre Nosotros</h4>
                    <ul>
                        <li>
                            <Link aria-label="nosotros" to="/nosotros">Nosotros</Link>
                        </li>
                        <li>
                            <Link aria-label="actividades" to="/actividades">Actividades</Link>
                        </li>
                        <li>
                            <Link aria-label="contacto" to="/Contact">Contacto</Link>
                        </li>
                    </ul>
                </nav>
                <address className="footer__about-information">
                    <h4 className="footer__title">Encuéntranos</h4>
                    <p>
                        Avenida Costanera.<br/>
                        Las colinas 1234, Costanera
                    </p>
                    <strong>
                        +56 9 1234 5678
                    </strong>
                </address>
            </div>
        </footer>
    )
}