import React, { useState } from 'react'
import { MdMenu } from 'react-icons/md'
import { NavLink, Link } from 'react-router-dom'
import { LogoMS } from '../commons/icons'

interface Header {
    color?: 'white' | 'transparent'
}

export const Header:React.FC<Header> = ({ color = "white" }) => {

    const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false)
    const [headerColor, setHeaderColor] = useState<string>(color)

    const handleMenuState = () => setIsMenuOpen(!isMenuOpen)

    return (
        <header className={`header header--${headerColor}`}>
            <div className="header__container">

                <Link to="/home" className={`header__logo`}>
                    <LogoMS />
                </Link>

                <nav className="header__nav">
                    <ul className="relative flex flex-row align-center justify-center header__nav-list">
                        <li>
                            <NavLink activeClassName="header__nav--active"  to="/nosotros">Nosotros</NavLink>
                        </li>
                        <li>
                            <NavLink activeClassName="header__nav--active" to="/actividades">Actividades</NavLink>
                        </li>
                        <li>
                            <NavLink activeClassName="header__nav--active" to="/contacto">Contacto</NavLink>
                        </li>
                    </ul>
                </nav>

                <menu className="header__menu">
                    <MdMenu className="header__menu-icon" onClick={handleMenuState} />
                    <nav className={`flex flex-col align-center justify-center header__menu-nav header__menu-nav--${isMenuOpen ? 'is-open' : 'is-close'}`}>
                        <h3 className="header__menu-nav-title">Menu</h3>
                        <ul className="relative flex flex-col align-center justify-center header__menu-nav-list">
                            <li>
                                <NavLink activeClassName="header__nav--active" to="/nosotros">Nosotros</NavLink>
                            </li>
                            <li>
                                <NavLink activeClassName="header__nav--active" to="/actividades">Actividades</NavLink>
                            </li>
                            <li>
                                <NavLink activeClassName="header__nav--active" to="/contacto">Contacto</NavLink>
                            </li>
                        </ul>
                    </nav>
                </menu>
            </div>
        </header>
    )
}