import React from 'react'

interface Image {
    src: string,
    caption: string,
    alt: string
}
export const Image:React.FC<Image> = ({ caption, src, alt }) => {

    return (
        <figure className="image">
            <img src={src} alt={alt} />
            <figcaption className="image__caption">
                { caption }
            </figcaption>
        </figure>
    )

}