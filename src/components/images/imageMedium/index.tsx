import React from 'react'

interface ImageMedium {
    src: string,
    alt: string
}
export const ImageMedium:React.FC<ImageMedium> = ({ src, alt }) => {

    return (
        <figure className="image-medium">
            <img src={src} alt={alt} />
        </figure>
    )

}