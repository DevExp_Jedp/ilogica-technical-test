import React, { MouseEventHandler } from 'react'
import { MdArrowBack } from 'react-icons/md'
import { screen, render, fireEvent } from '@testing-library/react'
import { Button } from './index'

describe("Tests for the Common component <Button />", () => {

    const text = "Hello world"
    const color = "red"
    const action:MouseEventHandler = jest.fn<MouseEventHandler, []>()
    const minWidth = 160

    beforeEach(() => {
        render(<Button color={color} text={text} minWidth={minWidth} onClick={action} Icon={MdArrowBack} />)
    })

    test("The component renders correctly", () => {
        expect(screen.getByText(text).tagName.toLowerCase()).toBe('button')
        expect(screen.getByText(text).classList.contains(`button--${color}`)).toBeTruthy()
        expect(screen.getByText(text).lastChild?.nodeName.toLowerCase()).toBe('svg')
        expect(screen.getByText(text).style.minWidth).toBe(`${minWidth}px`)
    })

    test("The components functionality works correctly", () => {
        const btn = screen.getByText(text)
        btn && fireEvent.click(btn)        
        btn && fireEvent.click(btn)
        expect(action).toHaveBeenCalledTimes(2)        
    })
})