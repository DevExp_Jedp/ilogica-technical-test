import React from 'react'
import { MouseEventHandler } from 'react'
import { IconType } from 'react-icons'

import { COLORS } from '../../../../utils/colors'

interface Button {
    text: string,
    Icon?: IconType,
    color?: COLORS,
    onClick: MouseEventHandler,
    minWidth?: number 
}

export const Button:React.FC<Button> = ({ text, Icon, color = 'red', onClick, minWidth = 160 }) => {

    return (
        <button 
            aria-label={text} 
            tabIndex={0} 
            className={`button button--${color}`} 
            onClick={onClick}
            style={{
                minWidth: `${minWidth}px`
            }}
        >
            {
                text
            }
            {
                Icon && <Icon />
            }
        </button>
    )
}