import React from 'react'
import { Link as LinkRouter} from 'react-router-dom'

import { COLORS } from '../../../../utils/colors'

interface Link {
    to: string,
    color: COLORS
}

export const Link:React.FC<Link> = ({ to, color = 'red', children }) => {

    return (
        <LinkRouter 
            tabIndex={0} 
            className={`link link--${color}`} 
            to={to}
        >
            {
                children
            }

        </LinkRouter>
    )
}