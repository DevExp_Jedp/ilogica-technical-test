import React from 'react'
import { MemoryRouter } from 'react-router-dom'
import { MdArrowBack } from 'react-icons/md'
import { screen, render } from '@testing-library/react'
import { Link } from './index'

describe("Tests for the Common component <Button />", () => {

    const to = "/home"
    const color = "blue"
    const text = "Go Back"

    beforeEach(() => {
        render(
            <MemoryRouter>
                <Link to={to} color={color}>
                    <MdArrowBack />
                    <p>{text}</p>
                </Link>
            </MemoryRouter>
        )
    })

    test("The component renders correctly", () => {
        expect(screen.getByText(text).parentElement?.tagName.toLowerCase()).toBe('a')
        expect(screen.getByText(text).tagName.toLowerCase()).toBe('p')
        expect(screen.getByText(text).parentElement?.classList.contains(`link--${color}`)).toBeTruthy()
        expect(screen.getByText(text).parentElement?.children[0].tagName.toLowerCase()).toBe('svg')
    })
})