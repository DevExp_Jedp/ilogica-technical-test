import React, { MouseEventHandler } from 'react'
import { COLORS } from '../../../utils/colors'

interface Label {
    isSelected?: boolean,
    text: string,
    onClick: MouseEventHandler,
    color?: COLORS
}

export const Label:React.FC<Label> = ({ isSelected = false, text, onClick, color = "blue" }) => {

    return (
        <button onClick={onClick} className={`label label--${color} ${isSelected && ('label--is-selected')} `}>
            {
                text
            }
        </button>
    )
}