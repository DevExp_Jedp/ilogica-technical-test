import React from 'react'
import { COLORS } from '../../../../utils/colors'

interface IconSocialMedia {
    title: string,
    to: string,
    color?: COLORS,
}

export const IconSocialMedia:React.FC<IconSocialMedia> = ({ to, title, color = 'red', children }) => {

    return (
        <a href={to} aria-label={title} className={`icon-social-media icon-social-media--${color}`}>
            {
                children
            }
        </a>
    )
}