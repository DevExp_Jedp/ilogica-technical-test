import React from 'react'

interface Quote {
    text: string
}

export const Quote:React.FC<Quote> = ({ text }) => {

    return (
        <article tabIndex={0} className="relative flex align-center justify-center quote">
            <img src="/public/images/social-media/quote-start.svg" tabIndex={-1} className="quote__quote-start" />
            <img src="/public/images/social-media/quote-end.svg" tabIndex={-1} className="quote__quote-end" />
            <p className="font-bold">
                {text}
            </p>
        </article>
    )

}