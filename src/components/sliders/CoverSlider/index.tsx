import React from 'react'
import SwiperCore, { A11y, Pagination  } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'


SwiperCore.use([A11y, Pagination])
export const CoverSlider:React.FC = ({ children }) => {

    return (
        <Swiper
            tag="article"
            className="container-cover-slider"
            spaceBetween={0}
            slidesPerView={1}
            wrapperTag="ol"
            pagination={{ clickable: true, el:'.cover-slider__pagination', bulletClass: "cover-slider__pagination-bullet", bulletElement: 'li', bulletActiveClass: 'cover-slider__pagination-bullet--active' }}
            lazy={true}
        >

            {
                children && (
                    React.Children.map(children, (Child, i) => (
                        React.isValidElement<React.ReactNode>(Child) && (
                            <SwiperSlide tag='li'>
                                <Child.type {...Child.props} key={i} />
                            </SwiperSlide>
                        )
                    ))
                )
            }
            

            <ol className="cover-slider__pagination" />

        </Swiper>
    )
}