import React,  { useState, useEffect } from 'react'
import SwiperCore, { A11y, Navigation  } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import { MdKeyboardBackspace } from 'react-icons/md'


SwiperCore.use([A11y, Navigation])

export const PartnersSlider:React.FC = ({ children }) => {

    const [slidesPerView, setSlidesPerView] = useState<number>(1.8)
    const [widthWindow, setWidthWindow] = useState<number>(0)

    const handleSlidesPerView = () => {
        widthWindow > 720 && (setSlidesPerView(2.5))
        widthWindow > 1080 && (setSlidesPerView(4))
        widthWindow > 1440 && (setSlidesPerView(5.5))
    }

    useEffect(() => {
        const { innerWidth } = window
        setWidthWindow(innerWidth)
        handleSlidesPerView()
    }, [widthWindow])

    return (
        <Swiper
            tag="article"
            className="container-partners-slider"
            navigation={{ nextEl: '.partners-slider__button-next', prevEl: '.partners-slider__button-prev', lockClass: '.partners-slider__button--disabled' }}
            spaceBetween={30}
            slidesPerView={slidesPerView}
            wrapperTag="ol"
            lazy={true}
        >

            {
                children && (
                    React.Children.map(children, (Child, i) => (
                        React.isValidElement<React.ReactNode>(Child) && (
                            <SwiperSlide tag='li'>
                                <Child.type {...Child.props} key={i} />
                            </SwiperSlide>
                        )
                    ))
                )
            }
            

            <div className="partners-slider__buttons">
                <button className="partners-slider__button partners-slider__button-prev">
                    <MdKeyboardBackspace />
                </button>
                <button className="partners-slider__button partners-slider__button-next">
                    <MdKeyboardBackspace />
                </button>
            </div>

        </Swiper>
    )
}