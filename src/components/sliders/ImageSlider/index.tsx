import React,  { useState, useEffect } from 'react'
import SwiperCore, { A11y, Navigation  } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import { MdKeyboardBackspace } from 'react-icons/md'


SwiperCore.use([A11y, Navigation])


export const ImageSlider:React.FC = ({ children }) => {

    return (
        <Swiper
            tag="article"
            className="container-images-slider"
            navigation={{ nextEl: '.images-slider__button-next', prevEl: '.images-slider__button-prev', lockClass: '.images-slider__button--disabled' }}
            spaceBetween={0}
            slidesPerView={1}
            wrapperTag="ol"
            lazy={true}
        >
            

            {
                children && (
                    React.Children.map(children, (Child, i) => (
                        React.isValidElement<React.ReactNode>(Child) && (
                            <SwiperSlide tag='li'>
                                <Child.type {...Child.props} key={i} />
                            </SwiperSlide>
                        )
                    ))
                )
            }

            <div className="images-slider__buttons">
                <button className="images-slider__button images-slider__button-prev">
                    <MdKeyboardBackspace />
                </button>
                <button className="images-slider__button images-slider__button-next">
                    <MdKeyboardBackspace />
                </button>
            </div>

        </Swiper>
    )
}