import React from 'react'
import { Link } from '../../commons/buttons/link'
import { MdArrowForward } from 'react-icons/md'

import { getShortText } from '../../../utils/getShortText'

interface ActivityCard {
    id: number,
    date: string,
    category: number,
    title: string,
    abstract: string,
    imageUrl: string
}


export const ActivityCard:React.FC<ActivityCard> = ({ abstract, category, date, id, imageUrl, title }) => {

    return (
        <article tabIndex={0} className="inline-flex flex-col align-start justify-between activity-card">

            <figure  className="activity-card__image">
                <img loading="lazy" alt={title} src={imageUrl} />
            </figure>
            <div className="activity-card__container">
                <small>
                    {date} - {`Category ${category}`} 
                </small>

                <div tabIndex={0} className="activity-card__text">
                    <h3>{title}</h3>
                    <p>
                        {getShortText({ text: abstract, end: 130 })}
                    </p>
                    <Link color="red" to={`/actividades/${id}`}>
                        Read more
                        <MdArrowForward />
                    </Link>
                </div>
            </div>
        </article>
    )

}