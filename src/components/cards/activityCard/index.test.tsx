import React from 'react'
import { MemoryRouter } from 'react-router-dom'
import { screen, render } from '@testing-library/react'
import { ActivityCard } from './index'

describe("Tests for the component <ActivityCard />", () => {
    const title = "Test title"
    const image = "public/image/backgrounds/bg-1.jpg"
    const category = 1
    const id = 2
    const date = "2021 nov"
    const abstract = "Some text of example"

    beforeEach(() => {
        render(
            <MemoryRouter>
                <ActivityCard 
                    title={title}
                    imageUrl={image}
                    abstract={abstract}
                    date={date}
                    id={id}
                    category={category}
                />
            </MemoryRouter>
        )
    })

    test("The component renders correclty", () => {
        const component = screen.getByText(title).parentElement?.parentElement?.parentElement
        expect(component?.tagName.toLowerCase()).toBe('article')
        expect(component?.classList.contains('activity-card')).toBeTruthy()
        expect(component?.children[0].tagName.toLowerCase()).toBe('figure')
        expect(screen.getByText(title).tagName.toLowerCase()).toBe('h3')
        expect(component?.children[0].children[0].getAttribute('src')).toBe(image)

    })

    test("The component functionality works correclty", () => {
        const linkContainer = screen.getByText(title).parentElement
        expect(linkContainer?.children[linkContainer.childElementCount - 1].getAttribute('href')).toBe(`/activity/${id}`)
    })






})