import React, { MouseEventHandler } from 'react'
import { MdArrowForward } from 'react-icons/md'
import { Button } from '../../commons/buttons/button'
import { COLORS } from '../../../utils/colors'
import { GET_MEDIA_QUERY_PICTURE } from '../../../utils/mediaQueriesPicture'

interface AdvantageCard {
    text: string,
    src: string,
    srcSet?: string[],
    action: MouseEventHandler,
    color?: COLORS
}

export const AdvantageCard:React.FC<AdvantageCard> = ({ src, srcSet, action, text, color = "red" }) => {

    return (
        <article tabIndex={0} aria-label={text} className={`advantage-card advantage-card--${color}`}>
            <picture className="advantage-card__image" aria-label={text}>
                {
                    srcSet && srcSet?.length > 0 && srcSet.map((src, i) => (
                        <source srcSet={src} key={i} media={GET_MEDIA_QUERY_PICTURE(i)} />
                    ))
                }
                <img src={src} alt={text} />
            </picture>
            <p className="advantage-card__text">
                {text}
            </p>
            <Button 
                onClick={action}
                text="Know More"
                Icon={MdArrowForward}
                color={color}
            />

        </article>
    )
}