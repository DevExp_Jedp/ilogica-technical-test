import React, { Component } from 'react'
import { screen, render, fireEvent } from '@testing-library/react'
import { AdvantageCard } from './index'
import { MouseEventHandler } from 'react'

describe("Tests for the component <AdvantageCard />", () => {
    const text = "Text test for tests"
    const image = "public/image/backgrounds/bg-1.jpg"
    const color = "blue"
    const srcSet = [
        "public/images/logos/kiosko/kiosko-logo.ppg",
        "public/images/logos/kiosko/kiosko-logo@2x.ppg"
    ]
    const action:MouseEventHandler = jest.fn<MouseEventHandler, []>() 

    beforeEach(() => {
        render(
            <AdvantageCard 
                action={action}
                src={image}
                text={text}
                color={color}
                srcSet={srcSet}
            />
        )
    })

    test("The component renders correclty", () => {
        const component = screen.getByText(text).parentElement

        expect(component?.tagName.toLowerCase()).toBe('article')
        expect(component?.children[0].tagName.toLowerCase()).toBe('picture')
        expect(component?.children[0].childElementCount).toBe(srcSet.length + 1)
        expect(component?.children[0].children[srcSet.length].tagName.toLowerCase()).toBe('img')
        expect(component?.children[0].children[srcSet.length].getAttribute('src')).toBe(image)
        expect(screen.getByText(text).tagName.toLowerCase()).toBe('p')
        expect(component?.children[component.childElementCount - 1].tagName.toLowerCase()).toBe('button')
    })

    test("The functionality works correctly" , () => {
        const component = screen.getByText(text).parentElement
        const button = component?.children[component.childElementCount - 1] 

        button && fireEvent.click(button)
        expect(action).toBeCalled()
        button && fireEvent.click(button)
        expect(action).toBeCalledTimes(2)

    })




})