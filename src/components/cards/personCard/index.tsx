import React from 'react'

interface PersonCard {
    name: string,
    abstract: string,
    imageUrl: string
}

export const PersonCard:React.FC<PersonCard> = ({ abstract, imageUrl, name }) => {

    return (
        <article className="inline-flex flex-col align-center justify-between person-card">
            <figure aria-label={`An image of ${name}`} className="person-card__image">
                <img loading="lazy" src={imageUrl} alt={name} />
            </figure>
            <div className="flex flex-col align-center justify-center person-card__text">
                <strong className="person-card__name">{name}</strong>
                <p className="person-card__abstract">{abstract}</p>
            </div>
        </article>
    )
}