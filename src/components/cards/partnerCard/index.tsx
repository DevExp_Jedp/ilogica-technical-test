import React from 'react'
import { GET_MEDIA_QUERY_PICTURE } from '../../../utils/mediaQueriesPicture'


interface PartnerCard {
    src: string,
    srcSet?: string[],
    text: string
}

export const PartnerCard:React.FC<PartnerCard> = ({ src, text, srcSet }) => {

    
    return (
        <article aria-label={text} tabIndex={0} className="partner-card">
            <picture className="partner-card__image" aria-label={text}>
                {
                    srcSet && srcSet?.length > 0 && srcSet.map((src, i) => (
                        <source srcSet={src} key={i} media={GET_MEDIA_QUERY_PICTURE(i)} />
                    ))
                }
                <img src={src} alt={text} />
            </picture>
        </article>
    )
} 