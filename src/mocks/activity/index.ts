import { Activity } from '../../utils/types/activity'

type ActivityData = {
    [key:string]: Activity[]
}

export const activtyData:ActivityData = {
    "Trips": [
        {
            id: 1,
            abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
            cover: "/public/images/backgrounds/bg-2.jpg",
            date: "28 nov 2021",
            description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
            images: ["/public/images/backgrounds/bg-3.jpg", "/public/images/backgrounds/bg-5.jpg"],
            title: "Lorem Ipsum Dolor Sit Amet",
            quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
            time: "1 min"
        },
        {
            id: 2,
            abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
            cover: "/public/images/backgrounds/bg-4.jpg",
            date: "28 nov 2021",
            description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
            images: ["/public/images/backgrounds/bg-1.jpg", "/public/images/backgrounds/bg-2.jpg"],
            title: "Lorem Dolor Sit Amet",
            quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
            time: "2 min"
        },
        {
            id: 3,
            abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
            cover: "/public/images/backgrounds/bg-1.jpg",
            date: "28 nov 2021",
            description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
            images: ["/public/images/backgrounds/bg-5.jpg", "/public/images/backgrounds/bg-3.jpg"],
            title: "Lorem Ipsum Dolor Sit Amet",
            quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
            time: "3 min"
        },
        {
            id: 4,
            abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
            cover: "/public/images/backgrounds/bg-2.jpg",
            date: "28 nov 2021",
            description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
            images: ["/public/images/backgrounds/bg-3.jpg", "/public/images/backgrounds/bg-5.jpg"],
            title: "Lorem Ipsum Dolor Sit Amet",
            quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
            time: "1 min"
        },
        {
            id: 5,
            abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
            cover: "/public/images/backgrounds/bg-3.jpg",
            date: "28 nov 2021",
            description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
            images: ["/public/images/backgrounds/bg-3.jpg", "/public/images/backgrounds/bg-5.jpg"],
            title: "Lorem Ipsum Dolor Sit Amet",
            quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
            time: "1 min"
        }
    ],
    "Free Time": [
        {
            id: 6,
            cover: "/public/images/backgrounds/bg-1.jpg",
            abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
            date: "28 nov 2021",
            description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
            images: ["/public/images/backgrounds/bg-3.jpg", "/public/images/backgrounds/bg-5.jpg"],
            title: "Lorem Ipsum",
            quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
            time: "1 min"
        },
        {
            id: 7,
            cover: "/public/images/backgrounds/bg-2.jpg",
            abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
            date: "28 nov 2021",
            description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
            images: ["/public/images/backgrounds/bg-3.jpg", "/public/images/backgrounds/bg-5.jpg"],
            title: "Lorem Ipsum",
            quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
            time: "1 min"
        },
        {
            id: 8,
            cover: "/public/images/backgrounds/bg-3.jpg",
            abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
            date: "28 nov 2021",
            description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
            images: ["/public/images/backgrounds/bg-4.jpg", "/public/images/backgrounds/bg-5.jpg"],
            title: "Lorem Ipsum",
            quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
            time: "1 min"
        }
    ],
    "Hobbies": [
        {
            id: 9,
            cover: "/public/images/backgrounds/bg-5.jpg",
            abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
            date: "28 nov 2021",
            description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
            images: ["/public/images/backgrounds/bg-4.jpg", "/public/images/backgrounds/bg-5.jpg"],
            title: "Lorem Ipsum Dolor Sit",
            quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
            time: "2 min"
        },
        {
            id: 10,
            cover: "/public/images/backgrounds/bg-3.jpg",
            abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
            date: "30 nov 2021",
            description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
            images: ["/public/images/backgrounds/bg-4.jpg", "/public/images/backgrounds/bg-5.jpg"],
            title: "Lorem Ipsum Dolor Sit",
            quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
            time: "3 min"
        },
        {
            id: 11,
            cover: "/public/images/backgrounds/bg-5.jpg",
            abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
            date: "29 nov 2021",
            description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
            images: ["/public/images/backgrounds/bg-4.jpg", "/public/images/backgrounds/bg-5.jpg"],
            title: "Lorem Ipsum Dolor Sit",
            quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
            time: "3 min"
        }
    ]
}

export const activities:Activity[] = [
    {
        id: 1,
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        cover: "/public/images/backgrounds/bg-2.jpg",
        date: "28 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-3.jpg", "/public/images/backgrounds/bg-5.jpg"],
        title: "Lorem Ipsum Dolor Sit Amet",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "1 min"
    },
    {
        id: 2,
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        cover: "/public/images/backgrounds/bg-4.jpg",
        date: "28 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-1.jpg", "/public/images/backgrounds/bg-2.jpg"],
        title: "Lorem Dolor Sit Amet",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "2 min"
    },
    {
        id: 3,
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        cover: "/public/images/backgrounds/bg-1.jpg",
        date: "28 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-5.jpg", "/public/images/backgrounds/bg-3.jpg"],
        title: "Lorem Ipsum Dolor Sit Amet",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "3 min"
    },
    {
        id: 4,
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        cover: "/public/images/backgrounds/bg-2.jpg",
        date: "28 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-3.jpg", "/public/images/backgrounds/bg-5.jpg"],
        title: "Lorem Ipsum Dolor Sit Amet",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "1 min"
    },
    {
        id: 5,
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        cover: "/public/images/backgrounds/bg-3.jpg",
        date: "28 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-3.jpg", "/public/images/backgrounds/bg-5.jpg"],
        title: "Lorem Ipsum Dolor Sit Amet",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "1 min"
    },
    {
        id: 6,
        cover: "/public/images/backgrounds/bg-1.jpg",
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        date: "28 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-3.jpg", "/public/images/backgrounds/bg-5.jpg"],
        title: "Lorem Ipsum",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "1 min"
    },
    {
        id: 7,
        cover: "/public/images/backgrounds/bg-2.jpg",
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        date: "28 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-3.jpg", "/public/images/backgrounds/bg-5.jpg"],
        title: "Lorem Ipsum",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "1 min"
    },
    {
        id: 8,
        cover: "/public/images/backgrounds/bg-3.jpg",
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        date: "28 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-4.jpg", "/public/images/backgrounds/bg-5.jpg"],
        title: "Lorem Ipsum",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "1 min"
    },
    {
        id: 9,
        cover: "/public/images/backgrounds/bg-5.jpg",
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        date: "28 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-4.jpg", "/public/images/backgrounds/bg-5.jpg"],
        title: "Lorem Ipsum Dolor Sit",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "2 min"
    },
    {
        id: 10,
        cover: "/public/images/backgrounds/bg-3.jpg",
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        date: "30 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-4.jpg", "/public/images/backgrounds/bg-5.jpg"],
        title: "Lorem Ipsum Dolor Sit",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "3 min"
    },
    {
        id: 11,
        cover: "/public/images/backgrounds/bg-5.jpg",
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        date: "29 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-4.jpg", "/public/images/backgrounds/bg-5.jpg"],
        title: "Lorem Ipsum Dolor Sit",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "3 min"
    }
]

export const relatedActivities:Activity[] = [
    {
        id: 1,
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        cover: "/public/images/backgrounds/bg-2.jpg",
        date: "28 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-3.jpg", "/public/images/backgrounds/bg-5.jpg"],
        title: "Lorem Ipsum Dolor Sit Amet",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "1 min"
    },
    {
        id: 2,
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        cover: "/public/images/backgrounds/bg-4.jpg",
        date: "28 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-1.jpg", "/public/images/backgrounds/bg-2.jpg"],
        title: "Lorem Dolor Sit Amet",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "2 min"
    },
    {
        id: 3,
        abstract: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis, maxime dolores, aut laboriosam consectetur nobis repudiandae perspiciatis eius a, totam ad! Dignissimos alias saepe aspernatur, ipsam odio minima omnis accusantium quibusdam, dolores sunt consectetur corrupti dolorem nobis? Magni incidunt possimus laboriosam libero, distinctio nam aut?",
        cover: "/public/images/backgrounds/bg-1.jpg",
        date: "28 nov 2021",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur provident, nisi, odit ipsam possimus, voluptas temporibus tempore excepturi expedita cum veritatis quasi illum autem doloremque nihil omnis repellendus voluptate blanditiis inventore reprehenderit minima rerum ipsum facilis! Quisquam inventore quia dolorem enim, molestiae error est tenetur facilis perspiciatis, culpa vero quas! Sint, quod, eveniet consequuntur ullam tempora porro molestiae vero qui eum ipsum laborum distinctio hic veritatis voluptas possimus soluta quos autem. Vel quae odio facere, sed accusantium, hic ex quidem cum quam officiis numquam nihil. Corporis officia modi error aspernatur tempora voluptatum quae impedit explicabo! Distinctio consectetur, enim qui quia magnam at ipsa eligendi labore, facere necessitatibus maxime nisi sint. Quasi nobis sed architecto sunt aperiam maxime iste, sequi natus!",
        images: ["/public/images/backgrounds/bg-5.jpg", "/public/images/backgrounds/bg-3.jpg"],
        title: "Lorem Ipsum Dolor Sit Amet",
        quote: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur mollitia, commodi ab nulla assumenda delectus.",
        time: "3 min"
    }
]