import { Person } from '../../utils/types/person'

type PeopleData = {
    [key:string]: Person[]
}

export const peopleData:PeopleData = {
    "Technology": [
        {
            id: 1,
            image: "public/images/people/people-1.jpg",
            name: "Joe Does",
            profession: "Frontend Developer"
        },
        {
            id: 2,
            image: "public/images/people/people-2.jpg",
            name: "Stepehn Down",
            profession: "Backend Developer"
        },
        {
            id: 3,
            image: "public/images/people/people-3.jpg",
            name: "Kyrie Jhonson",
            profession: "Data Scientis"
        },
        {
            id: 4,
            image: "public/images/people/people-4.jpg",
            name: "Luka Charles",
            profession: "Data Architect"
        },
        {
            id: 5,
            image: "public/images/people/people-5.jpg",
            name: "Enzo Days",
            profession: "Developer"
        },
        {
            id: 6,
            image: "public/images/people/people-1.jpg",
            name: "Rudy Body",
            profession: "UX Designer"
        },
    ],
    "Design": [
        {
            id: 1,
            image: "public/images/people/people-3.jpg",
            name: "Joe Does",
            profession: "Branding Designer"
        },
        {
            id: 2,
            image: "public/images/people/people-1.jpg",
            name: "Stepehn Down",
            profession: "UI/UX Designer"
        },
        {
            id: 3,
            image: "public/images/people/people-4.jpg",
            name: "Kyrie Jhonson",
            profession: "Information Architecture"
        },
        {
            id: 4,
            image: "public/images/people/people-5.jpg",
            name: "Luka Charles",
            profession: "Social Media Design"
        },
    ],
    "Bussines": [
        {
            id: 1,
            image: "public/images/people/people-2.jpg",
            name: "Joe Does",
            profession: "Frontend Developer"
        },
        {
            id: 2,
            image: "public/images/people/people-3.jpg",
            name: "Stepehn Down",
            profession: "Backend Developer"
        },
        {
            id: 3,
            image: "public/images/people/people-4.jpg",
            name: "Kyrie Jhonson",
            profession: "Data Scientis"
        },
        {
            id: 4,
            image: "public/images/people/people-1.jpg",
            name: "Luka Charles",
            profession: "Data Architect"
        },
        {
            id: 5,
            image: "public/images/people/people-5.jpg",
            name: "James Duh",
            profession: "Bussines Engineer"
        },
    ]
}