import React, { useState, useEffect } from 'react'
import { Activity } from '../../utils/types/activity'
import { Label } from '../../components/commons/label'
import { ActivityCard } from '../../components/cards/activityCard'

interface ActivityList {
    data: {[key:string]: Activity[]}
}

export const ActivityList:React.FC<ActivityList> = ({ data }) => {

    const [categories, setCategories] = useState<string[]>([])
    const [currentCategory, setCurrentCategory] = useState<string>("Trips")
    
    useEffect(() => {
        const setData = async () => {
            await setCategories(Object.keys(data))
            await categories.length > 0 && setCurrentCategory("Trips")
        }
        setData()

    }, [])


    return (
        <section className="activity-list">
            <div className="activity-list__labels">
                {
                    categories.length > 0 && categories.map((category,i) => (
                        <Label 
                            key={i}
                            text={category}
                            onClick={() => setCurrentCategory(category)}
                            color="blue"
                            isSelected={currentCategory === category}
                        />
                    ))
                }
            </div>

            <ul className="activity-list__list">
                {
                    currentCategory.length > 0 && data[currentCategory].map(activity => (
                        <li key={activity.id}>
                            <ActivityCard 
                                id={activity.id}
                                imageUrl={activity.cover}
                                abstract={activity.abstract}
                                category={categories.indexOf(currentCategory)}
                                date={activity.date}
                                title={activity.title}
                            />
                        </li>  
                    ))
                }
            </ul>
            
        </section>
    )
}