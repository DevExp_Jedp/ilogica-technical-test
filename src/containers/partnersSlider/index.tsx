import React from "react"
import { PartnerCard } from "../../components/cards/partnerCard"
import { PartnersSlider as Slider } from "../../components/sliders/PartnersSlider"

export const PartnersSliderContainer:React.FC = () => {

    return (
        <section className="partners-slider-container">
            <h2 className="title-decoration">Lorem ipsum dolor sit amet.</h2>
            <Slider>
                <PartnerCard 
                    src="/public/images/logos/kiosko/kiosko-logo.png"
                    text="Kiosko"
                    srcSet={["/public/images/logos/kiosko/kiosko-logo@2x.png", "/public/images/logos/kiosko/kiosko-logo@3x.png"]}
                />
                <PartnerCard 
                    src="/public/images/logos/juegging/juegging.png"
                    text="juegging"
                    srcSet={["/public/images/logos/juegging/juegging@2x.png", "/public/images/logos/juegging/juegging@3x.png"]}
                />
                <PartnerCard 
                    src="/public/images/logos/riverbed/riverbed.png"
                    text="riverbed"
                    srcSet={["/public/images/logos/riverbed/riverbed@2x.png", "/public/images/logos/riverbed/riverbed@3x.png"]}
                />
                <PartnerCard 
                    src="/public/images/logos/wise/wise.png"
                    text="wise"
                    srcSet={["/public/images/logos/wise/wise@2x.png", "/public/images/logos/wise/wise@3x.png"]}
                />
                <PartnerCard 
                    src="/public/images/logos/kiosko/kiosko-logo.png"
                    text="Kiosko"
                    srcSet={["/public/images/logos/kiosko/kiosko-logo@2x.png", "/public/images/logos/kiosko/kiosko-logo@3x.png"]}
                />
            </Slider>
        </section>
    )
}