import React from "react";
import { MdSend } from 'react-icons/md'
import { EmailIcon } from '../../components/commons/icons'
import { Button } from '../../components/commons/buttons/button'

export const ContactContainer:React.FC = () => {

    return (
        <section className="contact-container">

            <article className="contact-container__text">
                <div className="contact-container__text-container">
                    <figure className="flex align-center justify-center">
                        <EmailIcon />  
                    </figure>
                    <h2>Contacto</h2>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid numquam libero omnis, molestiae pariatur neque officia est non ullam. Reiciendis.
                    </p>
                </div>

            </article>

            <article className="contact-container__form">
                <form>
                    <label className="form-input" htmlFor="name">
                        <p className="form-input__label">Nombre</p>
                        <input required type="text" placeholder="Write your name" id="name" />
                    </label>
                    <label className="form-input" htmlFor="email">
                        <p className="form-input__label">Email</p>
                        <input required type="email" placeholder="Write your email" id="email" />
                    </label>
                    <label className="form-input" htmlFor="message">
                        <p className="form-input__label">Mensaje</p>
                        <textarea required rows={6} id="message" placeholder="Write us whatever you want"></textarea>
                    </label>
                    <Button 
                        text="Enviar"
                        color="red"
                        Icon={MdSend}
                        onClick={() => {}}
                    />
                </form>
            </article>

        </section>
    )
}