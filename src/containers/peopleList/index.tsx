import React, { useState, useEffect } from 'react'
import { Person } from '../../utils/types/person'
import { Label } from '../../components/commons/label'
import { PersonCard } from '../../components/cards/personCard'

interface PeopleList {
    data: {[key:string]: Person[]}
}

export const PeopleList:React.FC<PeopleList> = ({ data }) => {

    const [people, setPeople] = useState<{[key:string]: Person[]}>(data)

    const [categories, setCategories] = useState<string[]>([])
    const [currentCategory, setCurrentCategory] = useState<string>("Technology")
    
    useEffect(() => {
        const setData = async () => {
            await setCategories(Object.keys(data))
            await categories.length > 0 && setCurrentCategory("Technology")
        }
        setData()

    }, [])


    return (
        <section className="people-list">
            <div className="people-list__labels">
                {
                    categories.length > 0 && categories.map((category,i) => (
                        <Label 
                            key={i}
                            text={category}
                            onClick={() => setCurrentCategory(category)}
                            color="blue"
                            isSelected={currentCategory === category}
                        />
                    ))
                }
            </div>

            <ul className="people-list__list">
                {
                    currentCategory.length > 0 && people[currentCategory].map(person => (
                        <li key={person.id}>
                            <PersonCard 
                                name={person.name}
                                abstract={person.profession}
                                imageUrl={person.image}
                            />
                        </li>  
                    ))
                }
            </ul>
            
        </section>
    )
}