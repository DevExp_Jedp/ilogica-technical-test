import React from "react";
import { FacebookIcon, TwitterIcon, WhatsappIcon } from '../../components/commons/icons'

export const SocialMediaContainer:React.FC = () => {

    return (
        <article className="social-media-container">
            <h3>Compartir</h3>
            <ul>
                <a href="https://facebook.com" target="_blank">
                    <li>
                        <div className="social-media-container__icon">
                            <FacebookIcon />
                        </div>
                        <strong>
                            Facebook
                        </strong>
                    </li>
                </a>

                <a href="https://twitter.com" target="_blank">
                    <li>
                        <div className="social-media-container__icon">
                            <TwitterIcon />
                        </div>
                        <strong>
                            Twitter
                        </strong>
                    </li>
                </a>

                <a href="https://whatsapp.com" target="_blank">
                    <li>
                        <div className="social-media-container__icon">
                            <WhatsappIcon />
                        </div>
                        <strong>
                            WhatsApp
                        </strong>
                    </li>
                </a>

            </ul>
        </article>
    )
}