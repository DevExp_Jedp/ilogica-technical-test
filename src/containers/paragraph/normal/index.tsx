import React from 'react'
import { MdArrowForward } from 'react-icons/md'
import { COLORS } from '../../../utils/colors'

import { Button } from '../../../components/commons/buttons/button'


interface ParagraphNormal {
    title: string,
    color?: COLORS,
}

export const ParagraphNormal:React.FC<ParagraphNormal> = ({ children, title, color = "dark" }) => {

    return (
        <article className={`paragraph paragraph--${color}`}>
            <h2 className="title-decoration">{title}</h2>
            {children}
        </article>
    )

}