import React from 'react'
import { MdArrowForward } from 'react-icons/md'
import { COLORS } from '../../../utils/colors'

import { Button } from '../../../components/commons/buttons/button'


interface ParagraphWithButton {
    title: string,
    paragraph: string,
    color?: COLORS,
    buttonColor?: COLORS,
    onClick: ()=> void
}

export const ParagraphWithButton:React.FC<ParagraphWithButton> = ({ paragraph, title, buttonColor = "red", color = "dark", onClick }) => {

    return (
        <article className={`paragraph paragraph--${color}`}>
            <h2 className="title-decoration">{title}</h2>
            <p>
                {paragraph}
            </p>
            <Button 
                text="See More"
                Icon={MdArrowForward}
                onClick={onClick}
                color={buttonColor}
            />
        </article>
    )

}