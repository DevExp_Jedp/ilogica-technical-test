import React from 'react'
import { Activity } from '../../utils/types/activity' 
import { ParagraphNormal } from '../../containers/paragraph/normal'
import { ActivityCard } from '../../components/cards/activityCard'

interface RelatedActivities {
    activities: Activity[]
}

export const RelatedActivities:React.FC<RelatedActivities> = ({ activities }) => {

    return (
        <article className="related-activities">
            <ParagraphNormal title="Lorem Ipsum" />

            <ul className="related-activities__list">

                {
                    activities.map(activity => (
                        <li key={activity.id}>
                            <ActivityCard 
                                id={activity.id}
                                title={activity.title}
                                abstract={activity.abstract}
                                date={activity.date}
                                category={1}
                                imageUrl={activity.cover}
                            />
                        </li>
                    ))
                }

            </ul>
        </article>
    )
}