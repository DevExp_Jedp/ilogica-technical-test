import React from "react";
import { AdvantageCard } from "../../components/cards/advantageCard"

export const AdvantagesContainer:React.FC = () => {

    return (
        <section className="advantages-container">
            <h2 className="title-decoration">Lorem ipsum dolor sit amet.</h2>
            <ul>
                <li>
                    <AdvantageCard 
                        action={() => {}}
                        text="LOREM IPSUM"
                        color="red"
                        src="public/images/hearts/hearts.png"
                        srcSet={[
                            "public/images/hearts/hearts@2x.png",
                            "public/images/hearts/hearts@3x.png",
                        ]}
                    />
                </li>
                <li>
                    <AdvantageCard 
                        action={() => {}}
                        text="LOREM IPSUM"
                        color="blue"
                        src="public/images/hands/hands.png"
                        srcSet={[
                            "public/images/hands/hands@2x.png",
                            "public/images/hands/hands@3x.png",
                        ]}
                    />
                </li>
                <li>
                    <AdvantageCard 
                        action={() => {}}
                        text="LOREM IPSUM"
                        color="orange"
                        src="public/images/briefcase/briefcase.png"
                        srcSet={[
                            "public/images/briefcase/briefcase@2x.png",
                            "public/images/briefcase/briefcase@3x.png",
                        ]}
                    />
                </li>
            </ul>

        </section>
    )
}