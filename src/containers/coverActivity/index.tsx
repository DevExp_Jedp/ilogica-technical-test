import React from 'react'
import { useHistory } from 'react-router-dom'
import { MdArrowForward } from 'react-icons/md'
import { Button } from '../../components/commons/buttons/button'

interface CoverActivity {
    id: number,
    title: string,
    abstract: string,
    image: string
}

export const CoverActivity:React.FC<CoverActivity> = ({ abstract, image, title, id }) => {

    const { push } = useHistory()

    return (
        <article className="cover-activity">
            <figure className="cover-activity__image">
                <img src={image} alt={title} />
            </figure>
            <div className="cover-activity__content">
                <h1 className="title-decoration">{title}</h1>
                <p>{abstract}</p>
                <Button
                    text="Know More"
                    color="red"
                    Icon={MdArrowForward}
                    onClick={() => push(`/activity/${id}`)}
                />
            </div>
        </article>
    )
}