import React from 'react'
import { useHistory } from 'react-router-dom'
import { Button } from '../../components/commons/buttons/button'

export const NotFoundContainer = () => {

    const { push } = useHistory()

    return (
        <section className="flex flex-col align-center justify-center not-found-container">
            <picture>
                <source media="(min-width: 720px)" srcSet="public/images/404/404@2x.png" />
                <source media="(min-width: 1080px)" srcSet="public/images/404/404@3x.png" />
                <img alt="not found iamge with code 404" srcSet="public/images/404/404.png"/>
            </picture>
            <h2>Página no encontrada</h2>

            <nav className="not-found-container__nav">
                <strong>¿Que estás buscando?</strong>
                <ul>
                    <li>
                        <Button 
                            text="Nosotros"
                            color="red"
                            onClick={() => push('/nosotros')}
                        />
                    </li>
                    <li>
                        <Button 
                            text="Actividades"
                            color="red"
                            onClick={() => push('/actividades')}
                        />
                    </li>
                    <li>
                        <Button 
                            text="Contacto"
                            color="red"
                            onClick={() => push('/contacto')}
                        />
                    </li>
                </ul>
            </nav>
        </section>
    )
}