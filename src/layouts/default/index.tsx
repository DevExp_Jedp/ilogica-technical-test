import React from 'react'
import { Header } from '../../components/header'
import { Footer } from '../../components/footer'

interface DefaultLayout {
    color?: 'transparent' | 'white'
}

export const DefaultLayout:React.FC<DefaultLayout> = ({ color = 'white', children }) => {

    return (
        <>
        <Header 
            color={color}
        />
        <main id="main-content">
            <div className="main-content__content">
                {
                    children   
                }
            </div>
        </main>
        <Footer />
        </>
    )
}