export type Partner = {
    id: number,
    name: string,
    image: string,
    srcSet: string[]
}