export type Activity = {
    id: number,
    title: string,
    description: string,
    abstract: string,
    quote?: string,
    date: string,
    cover: string,
    images: string[],
    time?: string, 
}