export const getShortText = (data: {text:string, end: number}): string => {
    const { text, end = 120} = data
    return text.substring(0, end) + '...'
} 