const MEDIA_QUERIES_PICTURES = [
    "(min-width: 420px)",
    "(min-width: 720px)",
    "(min-width: 920px)"
]

export const GET_MEDIA_QUERY_PICTURE = (index:number) =>  MEDIA_QUERIES_PICTURES[index] || MEDIA_QUERIES_PICTURES[2]