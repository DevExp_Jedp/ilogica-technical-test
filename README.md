# ilogica-technical-test

Technical test for ilogica, based on the creation of a website with a frontend library or framework JS, like React, Vue, Angular. 

## Getting started

The project was done in ReactJS with TypeScript, so the first that you may want to do is install all the dependencies with `npm install`, once installed you will be able to run the next commands:

- __Run the project__: `npm run dev`
- __Build the project__: `npm run build` (For run the build version of the project should be started with a server)
- __Tests the project__: `npm run test` 


## About The Project  

The stack that I use was React with TypeScript as the frontend library, I use Sass for styles and Jest with @testing-library for make some unit tests.

__About the Responsive__  
The project is full responsive, but not in real time, so if you want to see correctly some adaptation of the project in another size screen you should reload the project with the size of the window already setted it.


